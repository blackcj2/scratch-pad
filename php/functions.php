<?php

function doSomething() {
    echo "<h1>Something</h1>";
}

doSomething();
doSomething(); // Call it again

// Result:
// <h1>Something</h1><h1>Something</h1>

function doSomethingWith($name) {
    echo "Hello " . $name . "<br>";
}

doSomethingWith("Chris");
$myName = "John";
doSomethingWith($myName);

// Hello Chris
// Hello John

function doSomethingAndReturn($name) {
    return "Hello " . $name; 
}

doSomethingAndReturn("Chris");

// Does not echo!!!

$result = doSomethingAndReturn("Chris");
echo $result;







?>